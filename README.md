# Eagle example tutorial

This tutorial for *Eagle* is divided into 2 parts. First we create an example eagle-data-file from a given vcf.gz file and annotate meta data by extracting the information of a related alignment in bam format. In the second part we create a configuration and start the interface.

## Requirements

### Installation

First of all *Eagle* must be installed and the easiest for doing so is *conda*.
**Download and install [Miniconda (Python 3)](https://conda.io/miniconda.html).**

If conda is installed, it is a good idea to create seperate environments for each project, even if this is not required.
Optional: **Create and activate an environment for this tutorial and activate it by executing the following commands.**
```sh
$ conda create -n eagle_tutorial
$ . activate eagle_tutorial
```

Regardless of whether a custom environment has been created, Eagle can be installed.
**Install Eagle from the bioconda channel and its dependencies by executing the following command.**
```sh
$ conda install -c bioconda eagle
```
Now eagle is installed and can be invoked by

```sh
$ eagle
```

### Download example data

**Clone this repository and switch into the new directory**

```sh
$ git clone git@bitbucket.org:christopherschroeder/eagle_tutorial.git 
$ cd eagle_tutorial
```

Example data used in this tutorial consists of two directories *single_example* and *complete_example*. The example is a subset of the data used in [(Schramm et al. ,2015)](https://www.ncbi.nlm.nih.gov/pubmed/26121086). The genotypes were reduced to genes, that came up in this work and a calling quality greater 500.

## Create an eagle-data-file

Here we show, how to create a eagle-data-file from a given *vcf* and annotate meta information extracted from its alignment.
For this **please change the directory to *single_example*.**
```sh
$ cd single_example
```
Inside this folder, you find 4 subdirectories *bam*, *capturekit* , *eaglefiles* and *vcf*. Our first goal is to convert
*vcf/13264.vcf.gz* into an eagle-data-file.

### Convert the data
A single vcf files is converted to an eagle-data-file by
```sh
$ eagle convert {input.vcf} {outputdir}
```
where the information of each sample of *input.vcf* is transformed and stored as *outputdir/samplename.h5*.
The vcf was created via multi-sample-calling and contains the genotype information of three samples. In this tutorial, we only want to extract the gentoype information from sample *13264N*. We can do this by invoking *Eagle*'s convert command.
Please note: We provided the tab index (tbi) for the vcf only to suppress missing contig warnings. The index is not mandatory for covnersion and such warnings can be ignored.

**Execute the following command to generate the example eagle-data-file.**
```sh
$ eagle convert vcf/13264.vcf.gz eaglefiles --samples 13264N
```

*Eagle* now reads the variants and genotype information for sample *13264N* in *vcf/13264.vcf.gz* and creates the eagle-data-file.

### Extract meta information
In addition, every eagle-data-file can hold (any) meta information, which may for example be metrics from it's underlying read alignment. Eagle provides a feature to extract the most important metrices from a bam.
We want to get the metrics from *bam/13264N.bam* and store it within our created eagle-data-file.

**Execute the following command to extract and write metadata to the eagle-data-file.**
```sh
$ eagle extract bam/13264N.bam capturekit/agilent-sureselect-v5.bed --samplerate 0.01 -w eaglefiles/13264N.h5
```
Here we use the *extract* function to calculate the read count, the duplication rate, the mappingrate and the coverage and some additional metrics from a given bam.
Since we are dealing with targeted sequecing data, also a capture kit bed-file is mandatory. This bed-file defines the target sequencing regions and may be retrieved from the capture kits manufacturer.
 In the example data, we provide a reduced (to chromosome 1) bed-file from agilent-sureselect kit version 5 and the 20% downsampled read alignment of chromosome 1.
To decrease the calculation time we use the *--samplerate* parameter to only use 0.01 of all regions for an duplication rate and coverage estimation as well as the *-w* parameter to store the obtained meta info to our eagle-data-file, instead of printing a list.
Please note, that there are faster methods, such as *samtools count* or *bedtools intersect*, to retrieve the same meta information.

### Add meta information

Not all meta informations are included in the alignment, e.g. the *Disease* of the sample. *Disease* is an important meta tag and *Eagle* will provide groups of samples for selection for each existing *Disease*.
Meta information can also be manually added to a sample by

**Execute the following command to store the Disease=Healthy information**
```sh
$ eagle meta eaglefiles/13264N.h5 Disease -s Healthy
```
Here we use the *-s* parameter to store a value to the given meta name (*Disease*).
The meta information can also be stored by the *--storelist* parameter and a provided tab separated name/value list.
We can check the sample meta information by
```sh
$ eagle meta eaglefiles/13264N.h5
```
which prints all stored name/value pairs:

```
All Reads        2213064
Duplication Rate 0.084
Mapping Rate     0.916
Coverage         16.61
Cov >= 1         0.993
Cov >= 5         0.856
Cov >= 10        0.643
Cov >= 20        0.304
Cov >= 50        0.037
Cov >= 100       0.002
Disease          Healthy
_reads_on_x      0
_reads_on_y      0
```

Since we used a subsampling, that relies on randomly choosen regions, to extract the meta information your, results may vary at this point.
Each meta information will be shown *Eagle*'s statistic overview, except it's name starts with "_". These values are only for internal calculations. In our case *_reads_on_x* and *_reads_on_y* are read counts for the X and Y chromosome, which is required to estimate the gender.

## Setup and start the interface

We already saw how to process vcf files in order to create eagle-data-files. Now we are going to start the interface. For this **please change the direction to *complete_example* by**

```sh
$ cd complete_example
```

or if you are still in the subdirectory by

```sh
$ cd ../complete_example
```

Currently this folder only contains the subdirectories *eagle-files* containing 58 example eagle-data-files, each holding a subset of variants from chromosome 1 from the orginal files. We already set the *Disease* and extracted the bam metrics for each of these files. These files serve as a database for the following.

### Create the configuration
Running an Eagle server requires a configuration files. The configuration file includes 2 categories: *pathes* and *reference*.
In *pathes* you can set three directories:
- *snp* - the directory that contains the eagle-data-files (generated in the previous step)
- *bam* - the directory that contains corresponding bam files, where *samplename.bam* corresponds to *samplename.h5* in the *snp* directory (optional)
- *groups* - the directory that stores user defined sample groups

The *bam* entry is optional and since we have no bam files in this example, we provide *snp* and *groups*.
The *group* path refers to a directory, where user defined groups of samples are stored in txt format. These group-files are just lists of sample names in text format and can be stored, modified and deleted via the interface and are listed as selection options for the main analysis.

**Create a configuration file with following content and name it *tutorial.cfg*.**
```
[pathes]
snp: ./eaglefiles
group: ./

[reference]
version: hg19
```
Additionaly we set the reference sequence version, which is *hg19* for the example data.

### Run the server

Now that the configuration is set up, we can **start the interface by**

```sh
$ eagle interface -c tutorial.cfg
```
After a view seconds, a message appears telling us that the server is running on 127.0.0.1 and port 8000. We can access the interface by using the url http://127.0.0.1:8000.

License
----

MIT